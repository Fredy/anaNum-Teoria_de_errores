unit mError;

interface

uses Math;

function absError(r : real; c : real ) : real;
function relError(r : real; c : real ) : real;
function perError(r : real; c : real ) : real;

implementation

function absError(r : real; c : real ) : real;
begin
   absError := abs(r - c);
end;

function relError(r : real; c : real ) : real;
begin
   relError := abs((r - c) / r);
end;

function perError(r : real; c : real ) : real;
begin
   perError := abs((r - c) / r) * 100;
end;

end.
