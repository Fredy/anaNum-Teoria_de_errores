unit Unit1;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Grids,
   StdCtrls, ExtCtrls, Math, mTaylorSeries, mError;

type

   TSFunction = function(val : real; n :integer; actualSum : real) : real;
   { TForm1 }

   TForm1 = class(TForm)
      ShowEdRealv: TEdit;
      Label1: TLabel;
      SinButton: TButton;
      ExpButton: TButton;
      ErrorEdit: TEdit;
      ValEdit: TEdit;
      ErrorTable: TStringGrid;
      GroupBox1: TGroupBox;
      Absolute: TRadioButton;
      GroupBox2: TGroupBox;
      GroupBox3: TGroupBox;
      Relative: TRadioButton;
      Percent: TRadioButton;
      procedure ButtonGenDataClick(Sender: TObject);
      procedure CleanStringGrid();

   private
      { private declarations }
   public
      { public declarations }
   end;

var
   Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.ButtonGenDataClick(Sender: TObject);
var
   button: TButton;
   minErr, value, realVal, taylor, nextTaylor,
   aErr, rErr, pErr, aErrx, rErrx, pErrx: real;
   i : integer;
   errorF : ^real;
   taylorFun : TSFunction;
   stringList :TStringList;


begin
   if not(Sender is TButton) then
      exit;

   CleanStringGrid();

   button := Sender as TButton;

   if ValEdit.GetTextLen = 0 then
   begin
      ShowMessage('Ingrese un valor');
      exit;
   end;
   if ErrorEdit.GetTextLen = 0 then
   begin
      ShowMessage('Ingrese un error mínimo');
      exit;
   end;

   if (not TryStrToFloat(ValEdit.Text, value)) then
   begin
      stringList := TStringList.Create;
      ExtractStrings(['/'],[],PChar(ValEdit.Text), stringList);

      if (LowerCase(stringList[0]) = 'pi') then
         value := pi
      else
         value := StrToFloat(stringList[0]);
      value := value / StrToFloat(stringList[1]);
   end;

   minErr := StrToFloat(ErrorEdit.Text);

   case button.Tag of
      0 : begin
             taylorFun := @tsSinNextVal;
             realVal := sin(value);
          end;
      1 : begin
             taylorFun := @tsExpNextVal;
             realVal := exp(value);
          end;
   end;
   ShowEdRealv.Caption := FormatFloat('0.#######', realVal);
   
   if   Absolute.Checked then errorF := @aErr
   else if Relative.Checked then
   begin
      if (ShowEdRealv.Caption = '0') then
      begin
         ShowMessage('El valor real es 0, no se puede hallar valor relativo.' + sLineBreak + 'Division entre 0.');
         exit;
      end
      else
         errorF := @rErr
   end
   else if Percent.Checked  then
   begin
      if (ShowEdRealv.Caption = '0') then
      begin
         ShowMessage('El valor real es 0, no se puede hallar valor relativo porcentual.'+ sLineBreak + 'Division entre 0.s');
         exit;
      end
      else
         errorF := @pErr;
   end;


   ShowEdRealv.Caption := FormatFloat('0.#######', realVal);
   errorF^ := minErr;
   i := 0;
   nextTaylor := taylorFun(value, i, 0);
   while (errorF^ >= minErr) do
   begin
      taylor := nextTaylor;
      nextTaylor := taylorFun(value, i + 1, nextTaylor);
      aErr := absError(realVal, taylor);
      rErr := relError(realVal, taylor);
      pErr := perError(realVal, taylor);
      aErrx := absError(nextTaylor, taylor);
      rErrx := relError(nextTaylor, taylor);
      pErrx := perError(nextTaylor, taylor);

      if (ShowEdRealv.Caption = '0') then    // valor real == 0
      begin
            ErrorTable.InsertRowWithValues(i + 1, [IntToStr(i), FormatFloat('0.#######', taylor), FormatFloat('0.#######', aErr),
                                                   '-', '-', FormatFloat('0.#######', aErrx), '-', '-'])

      end
      else
         ErrorTable.InsertRowWithValues(i + 1, [IntToStr(i), FormatFloat('0.#######', taylor), FormatFloat('0.#######', aErr),
                                                FormatFloat('0.#######', rErr), FormatFloat('0.#######', pErr),
                                                FormatFloat('0.#######', aErrx), FormatFloat('0.#######', rErrx),
                                                FormatFloat('0.#######', pErrx)]);
      i := i + 1;
   end;

end;

procedure TForm1.CleanStringGrid;
var
   I: Integer;
begin
   for I := 0 to ErrorTable.ColCount - 1 do
      ErrorTable.Cols[I].Clear;
   ErrorTable.RowCount := 1;
end;


end.
