program PepitoGrillo;

uses Math, mTaylorSeries, mError;

procedure makeTableAS(rel : real; val : real; minErr : real);
var
   i             : integer;
   error, taylor, nextTayl : real;
begin
   error := minerr;
   i := 0;
   taylor := 0;
   nextTayl := tsSinNextVal(val, i, taylor);

   writeln('n', 'ST':7, 'Ea':11, 'Er':11, 'Erp':11, 'Ea*':12, 'Er*':13, 'Erp*':11);
   while (error >= minerr) do
   begin
      taylor := nextTayl;
      nextTayl := tsSinNextVal(val, i + 1, nextTayl);
      error := absError(rel, taylor);

      writeln(i, ' ', taylor:0:8, ' ', error:0:8, ' ', relError(rel, taylor):0:8,
              ' ', perError(rel, taylor):0:8, '% '
              , absError(nextTayl,taylor):0:8, ' ', relError(nextTayl,taylor):0:8,
              ' ', perError(nextTayl,taylor):0:8, '%');

      i := i + 1;
   end;
end;

procedure makeTablePS(rel : real; val : real; minErr : real);
var
   i             : integer;
   error, taylor, nextTayl : real;
begin
   error := minerr;
   i := 0;
   taylor := 0;
   nextTayl := tsSinNextVal(val, i, taylor);

   writeln('n', 'ST':7, 'Ea':11, 'Er':11, 'Erp':11, 'Ea*':12, 'Er*':13, 'Erp*':11);
   while (error >= minerr) do
   begin
      taylor := nextTayl;
      nextTayl := tsSinNextVal(val, i + 1, nextTayl);
      error := perError(rel, taylor);

      writeln(i, ' ', taylor:0:8, ' ',  absError(rel, taylor):0:8, ' ', relError(rel, taylor):0:8,
              ' ', error:0:8, '% '
              , absError(nextTayl,taylor):0:8, ' ', relError(nextTayl,taylor):0:8,
              ' ', perError(nextTayl,taylor):0:8, '%');

      i := i + 1;
   end;
end;


procedure makeTableAE(rel : real; val : real; minErr : real);
var
   i             : integer;
   error, taylor, nextTayl : real;
begin
   error := minerr;
   i := 0;
   taylor := 0;
   nextTayl := tsExpNextVal(val, i, taylor);

   writeln('n', 'ST':7, 'Ea':11, 'Er':11, 'Erp':11, 'Ea*':12, 'Er*':13, 'Erp*':11);
   while (error >= minerr) do
   begin
      taylor := nextTayl;
      nextTayl := tsExpNextVal(val, i + 1, nextTayl);
      error := absError(rel, taylor);

      writeln(i, ' ', taylor:0:8, ' ', error:0:8, ' ', relError(rel, taylor):0:8,
              ' ', perError(rel, taylor):0:8, '% '
              , absError(nextTayl,taylor):0:8, ' ', relError(nextTayl,taylor):0:8,
              ' ', perError(nextTayl,taylor):0:8, '%');

      i := i + 1;
   end;
end;

procedure makeTablePE(rel : real; val : real; minErr : real);
var
   i             : integer;
   error, taylor, nextTayl : real;
begin
   error := minerr;
   i := 0;
   taylor := 0;
   nextTayl := tsExpNextVal(val, i, taylor);

   writeln('n', 'ST':7, 'Ea':11, 'Er':11, 'Erp':11, 'Ea*':12, 'Er*':13, 'Erp*':11);
   while (error >= minerr) do
   begin
      taylor := nextTayl;
      nextTayl := tsExpNextVal(val, i + 1, nextTayl);
      error := perError(rel, taylor);

      writeln(i, ' ', taylor:0:6, ' ',  absError(rel, taylor):0:6, ' ', relError(rel, taylor):0:6,
              ' ', error:0:6, '% '
              , absError(nextTayl,taylor):0:6, ' ', relError(nextTayl,taylor):0:6,
              ' ', perError(nextTayl,taylor):0:6, '%');

      i := i + 1;
   end;
end;


//MAIN
const
   pi = 3.14159265359;
begin
   makeTableAS(sin(pi/2), pi/2, 0.00001);
   writeln('----------');
   makeTablePS(sin(pi/4), pi/4, 0.01);
   writeln('**********************');
   makeTableAE(exp(1), 1, 0.0001);
   writeln('----------');
   makeTablePE(exp(3), 3, 0.001); // ARREGLAR: muere aquí
end.
