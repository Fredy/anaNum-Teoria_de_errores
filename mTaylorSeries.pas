unit mTaylorSeries;

interface

uses Math, mMath;

// This functions calculates the next value produced by the series;
function tsSinNextVal(val : real; n : integer; actualSum: real) : real;
function tsExpNextVal(val : real; n : integer; actualSum: real) : real;

implementation

function tsSinNextVal(val : real; n : integer; actualSum: real) : real;
var
   res : real;
   tmp : integer;
begin
   tmp := 2 * n + 1;
   res := power(-1, n) / fact(tmp) * power(val, tmp) ;
   tSsinNextVal := actualSum + res;
end;

function tsExpNextVal(val : real; n : integer; actualSum: real) : real;
var
   res : real;
begin
   res := power(val, n) / fact(n);
   tSexpNextVal := actualSum + res;
end;

end.
